// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
      if (cordova.platformId === "ios" && window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    //$ionicConfigProvider.tabs.position('bottom');
    $ionicConfigProvider.navBar.alignTitle('center');
        $stateProvider

            // setup an abstract state for the tabs directive
            .state('tab', {
                url: '/tab',
                abstract: true,
                templateUrl: 'templates/tabs.html'
            })

            // Each tab has its own nav history stack:
            .state('tab.account1', {
                url: '/account1',
                views: {
                    'tab-account1': {
                        templateUrl: 'templates/tab-products.html',
                        controller: 'AccountCtrl'
                    }
                }
            })
            .state('tab.account2', {
                url: '/account2',
                views: {
                    'tab-account2': {
                        templateUrl: 'templates/tab-account.html',
                        controller: 'AccountCtrl'
                    }
                }
            })
            .state('tab.account3', {
                url: '/account3',
                views: {
                    'tab-account3': {
                        templateUrl: 'templates/tab-account.html',
                        controller: 'AccountCtrl'
                    }
                }
            })
            .state('tab.account4', {
                url: '/account4',
                views: {
                    'tab-account4': {
                        templateUrl: 'templates/tab-account.html',
                        controller: 'AccountCtrl'
                    }
                }
            })
            .state('tab.account5', {
                url: '/account5',
                views: {
                    'tab-account5': {
                        templateUrl: 'templates/tab-account.html',
                        controller: 'AccountCtrl'
                    }
                }
            })

            .state('login', {
                url: '/login',
                templateUrl: 'templates/log-in.html',
                controller: 'AccountCtrl'
            })

            .state('thankyou', {
                url: '/thankyou',
                templateUrl: 'templates/thank-you.html',
                controller: 'AccountCtrl'
            })

            .state('products', {
                url: '/products',
                templateUrl: 'templates/list-of-products.html',
                controller: 'AccountCtrl'
            })

            .state('pricelist', {
                url: '/pricelist',
                templateUrl: 'templates/interest-price-list.html',
                controller: 'AccountCtrl'
            })

            .state('payments', {
                url: '/payments',
                templateUrl: 'templates/payments.html',
                controller: 'AccountCtrl'
            })

            .state('register', {
                url: '/register',
                templateUrl: 'templates/register.html',
                controller: 'AccountCtrl'
            });

        $urlRouterProvider.otherwise('/login');
    });
