angular.module('starter.controllers', [])
.controller('AccountCtrl', function ($scope, $state) {
    $scope.goToHome = function() {
        $state.go('tab.account1');
    }

    $scope.goToRegister = function () {
        $state.go('register');
    }

    $scope.goToThankYou = function () {
        $state.go('thankyou');
    }

    $scope.goToPayments = function () {
        $state.go('payments');
    }

    $scope.goToInterestPricelist = function () {
        $state.go('pricelist');
    }

    $scope.pricelist = [
    {
        date: "6/1/2016",
        interestRate: "17.79405%"
    },
    {
        date: "5/1/2016",
        interestRate: "13.12405%"
    },
    {
        date: "4/1/2016",
        interestRate: "15.90124%"
    },
    {
        date: "3/1/2016",
        interestRate: "14.34242%"
    },
    {
        date: "2/1/2016",
        interestRate: "19.59889%"
    },
    {
        date: "1/1/2016",
        interestRate: "12.23443%"
    },
    {
        date: "6/1/2016",
        interestRate: "17.79405%"
    },
    {
        date: "6/1/2016",
        interestRate: "17.79405%"
    },
    {
        date: "6/1/2016",
        interestRate: "17.79405%"
    }
];

    $scope.paymentsList = ["Mpesa", "Airtel Money", "Bank Account"];

    $scope.clientSide = "Mpesa";
    $scope.phonenumber = "254726524721";
    $scope.amount = 2500;
})
.controller('SideMenuCtrl', function ($scope, $state, $ionicSideMenuDelegate) {
    var sideMenuVm = this;

    $ionicSideMenuDelegate.canDragContent(false);

    sideMenuVm.gotToLogin = function () {
        $state.go('login');
    }

    sideMenuVm.gotToRegister = function () {
        $state.go('register');
    }

    sideMenuVm.gotToProducts = function () {
        $state.go('products');
    }
});
